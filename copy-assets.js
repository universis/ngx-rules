const thisPackage = require('./package.json');
const path = require('path');
const rimraf = require('rimraf');
const copy = require('copy-concurrently');
// get source directory ./assets
const src = path.resolve(__dirname, 'src/lib/components/assets');
// get destination directory ../../dist/forms/assets
// check if package name uses scope
let packageName =thisPackage.name;
if (/^@/g.test(packageName)) {
    // get package name without scope
    packageName = thisPackage.name.split('/')[1];
}
/**
 * @type {{$schema: string, dest: string, lib: {entryFile: string}}}
 */
const ngPackagr = require('./ng-package.json');
dest = path.resolve(__dirname, `${ngPackagr.dest}/assets`);
// remove destination files
rimraf.sync(dest);
console.log('Copy Assets');
console.log('From: ' + src);
console.log('To: ' + dest);
// copy assets
copy(src, dest).then(() => {
    // copy assets completed
    return process.exit(0);
}).catch(err => {
    console.log('ERROR', err);
    // exit with error
    return process.exit(-2);
});
